package br.com.hermes.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class GenericDAO<PK, T> {
	
	private final EntityManagerFactory entityManagerFactory;
	//@PersistenceContext(unitName="hermes_unit")
	protected final EntityManager entityManager;
	
	public GenericDAO(){
		this.entityManagerFactory = Persistence.createEntityManagerFactory("hermes_unit");
		this.entityManager = this.entityManagerFactory.createEntityManager();
		System.out.println("teste");
	}
	
	public Session session(){
		return (Session) entityManager.getDelegate();
	}
	
	@Transactional
	public void persist(T entity){
		try {
			this.entityManager.getTransaction().begin();
			this.entityManager.merge(entity);
			this.entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findAll(){
		
		Query query = entityManager.createQuery("select u from " +getTypeClass().getName()+" u");
		List<T> users = query.getResultList();
		return users;
		
		//return entityManager.createQuery(("FROM" + getTypeClass().getName())).getResultList();
	}
	
	
	private Class<?> getTypeClass() {
        Class<?> clazz = (Class<?>) ((ParameterizedType) this.getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1];
        return clazz;
    }

}
