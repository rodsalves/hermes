package br.com.hermes.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.hermes.dao.interfaces.ActionDAOI;
import br.com.hermes.model.Action;

@Repository("actionDAO")
public class ActionDAO extends GenericDAO<Long, Action> implements ActionDAOI{
	
	public void persist(Action action){
		super.persist(action);
	}
	
	@SuppressWarnings("unchecked")
	public List<Action> returnActionsNotAccessed(){
		return entityManager.createQuery("select a from Action a where a.accessed = false").getResultList();
	} 

}
