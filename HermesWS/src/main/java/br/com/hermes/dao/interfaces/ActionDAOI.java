package br.com.hermes.dao.interfaces;

import java.util.List;

import br.com.hermes.model.Action;

public interface ActionDAOI {
	
	void persist(Action action);
	
	List<Action> findAll();

}
