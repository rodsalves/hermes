package br.com.hermes.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.hermes.dao.ActionDAO;
import br.com.hermes.model.Action;
import br.com.hermes.model.Item;

@Controller
@Scope("view")
public class ActionController {
	
	ActionDAO actionDAO = new ActionDAO();
	
	public void persist(Action action){
		actionDAO.persist(action);
	}
	
	public List<Action> find(){
		return actionDAO.findAll();
	}
	
	public List<Action> listActionsNotAccessed(){
		return actionDAO.returnActionsNotAccessed();
	}
	
	public List<Item> list(){
		
		List<Item> list = new ArrayList<Item>();
		
		Item item = new Item();
		
		item.setComprado(true);
		item.setProduto("Café");
		item.setQuantidade(2);
		
		Item item2 = new Item();
		
		item2.setComprado(true);
		item2.setProduto("Arroz");
		item2.setQuantidade(3);
		
		list.add(item);
		list.add(item2);
		
		return list;
	}
}
