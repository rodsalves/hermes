package br.com.hermes.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Item {
	
	private String produto;
	private int quantidade;
	private Boolean comprado;
	
	public Item(){}
	
	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
	
	public Boolean getComprado() {
		return comprado;
	}
	public void setComprado(Boolean comprado) {
		this.comprado = comprado;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	

}
