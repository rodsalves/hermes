package br.com.hermes.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table
public class Action {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long pk;
	@Column
	private Long client;
	@Column
	@Temporal(TemporalType.DATE)
	private Date date_register;
	@Column
	private String user;
	@Column
	@Temporal(TemporalType.DATE)
	private Date date_init;
	@Column
	@Temporal(TemporalType.DATE)
	private Date date_end;
	@Column
	private String repetition;
	@Column
	private String action;
	@Column
	private String recipient_email;
	@Column
	private Boolean accessed;
	
	
	public Action(){
		
	}
	

	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}


	public Long getClient() {
		return client;
	}


	public void setClient(Long client) {
		this.client = client;
	}


	public Date getDate_register() {
		return date_register;
	}


	public void setDate_register(Date date_register) {
		this.date_register = date_register;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public Date getDate_init() {
		return date_init;
	}


	public void setDate_init(Date date_init) {
		this.date_init = date_init;
	}


	public Date getDate_end() {
		return date_end;
	}


	public void setDate_end(Date date_end) {
		this.date_end = date_end;
	}


	public String getRepetition() {
		return repetition;
	}


	public void setRepetition(String repetition) {
		this.repetition = repetition;
	}


	public String getRecipient_email() {
		return recipient_email;
	}


	public void setRecipient_email(String recipient_email) {
		this.recipient_email = recipient_email;
	}


	public Boolean getAccessed() {
		return accessed;
	}


	public void setAccessed(Boolean accessed) {
		this.accessed = accessed;
	}
}
