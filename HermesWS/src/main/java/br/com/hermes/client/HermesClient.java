package br.com.hermes.client;

import br.com.hermes.model.Action;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class HermesClient {

	public static void main(String[] args){

		teste2();

	}

	public static void teste2(){
		Action action = new Action();
		action.setPk(null);
		action.setAction("Yan Viado");
		ClientConfig clientConfig = new DefaultClientConfig();

		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);

		Client client = Client.create(clientConfig);

		WebResource webResource = client.resource("http://localhost:8080/HermesWS/rest/hermes/registerAction");

		ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, action);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}
		String output = response.getEntity(String.class);
		
		System.out.println(output);
	}
}
