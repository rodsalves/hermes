package br.com.hermes.ws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.hermes.controller.ActionController;
import br.com.hermes.model.Action;
import br.com.hermes.model.Item;

@Path("/hermes")
public class HermesServer {
	
	@GET
	@Produces("text/plain")
	@Path("/test")
	public String test(){
		return "Test OK";
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listAll")
	public List<Action> listar(){
		ActionController controller = new ActionController();
		List<Action> actions = controller.find();
		
		return actions;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listActionsNotAccessed")
	public List<Action> listActionsNotAccessed(){
		ActionController controller = new ActionController();
		List<Action> actions = controller.listActionsNotAccessed();
		
		return actions;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/teste")
	public List<Item> list(){
		ActionController controller = new ActionController();
		return controller.list();
	}
	
	@POST
	@Path("/registerAction")
	@Consumes(MediaType.APPLICATION_JSON)
	public String registerAction(Action action){
		
		try {
			ActionController controller = new ActionController();
			action.setAccessed(false);
			controller.persist(action);
			
			return "ok";
		} catch (Exception e) {
			return "error";
		}
	}
	
	@POST
	@Path("/updateAction")
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateAction(Action action){
		
		try {
			ActionController controller = new ActionController();
			controller.persist(action);
			
			return "ok";
		} catch (Exception e) {
			return "error";
		}
	}
}
